package com.tasco.sdmap.cli;

import java.io.File;
import java.io.IOException;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.ParseException;

import com.tasco.sdmap.cli.options.CliOptions;
import com.tasco.sdmap.cli.options.CliParser;
import com.tasco.sdmap.data.SaveFileProcessor;

public class Main {
    public static void main(final String[] args) {
        boolean verbose = false;

        try {
            verbose = CliParser.isVerboseSet(args);

            final SaveFileProcessor processor = new SaveFileProcessor(
                    new File(CliParser.parseCliForSaveFile(args)).toPath());

            processor.generateVisualizationFile(CliParser.parseCliForOutputFileName(args));

        } catch (final ParseException e) {
            final HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("sd-mapper", CliOptions.OPTIONS);
        } catch (final IOException e) {
            if (verbose) {
                e.printStackTrace();
            }
            System.out.println(e.getMessage());
        }
    }
}
