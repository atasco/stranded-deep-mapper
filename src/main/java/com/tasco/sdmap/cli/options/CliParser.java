package com.tasco.sdmap.cli.options;

import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.ParseException;

public class CliParser {
    public static String parseCliForSaveFile(final String[] cliArgs) throws ParseException {
        return new GnuParser().parse(CliOptions.OPTIONS, cliArgs).getOptionValue(CliOptions.FILE_OPTION);
    }

    public static String parseCliForOutputFileName(final String[] cliArgs) throws ParseException {
        return new GnuParser().parse(CliOptions.OPTIONS, cliArgs).getOptionValue(CliOptions.OUT_FILE_OPTION);
    }

    public static boolean isVerboseSet(final String[] cliArgs) throws ParseException {
        return new GnuParser().parse(CliOptions.OPTIONS, cliArgs).hasOption(CliOptions.VERBOSE_OPTION);
    }
}
