package com.tasco.sdmap.cli.options;

import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

@SuppressWarnings("static-access")
public class CliOptions {
    private static final String FILE_ARG_NAME = "save file";
    public static final String FILE_LONG_OPTION = "file";
    public static final String FILE_OPTION = "f";
    public static final String FILE_OPTION_DESC = "Path to Stranded Deep save file. Usually named Save.json.";

    private static final String OUT_ARG_NAME = "output filename";
    public static final String OUT_FILE_LONG_OPTION = "output";
    public static final String OUT_FILE_OPTION = "o";
    public static final String OUT_OPTION_DESC = "file name for output file";

    private static final String VERBOSE_OPTION_DESC = "verbose output";
    public static final String VERBOSE_OPTION = "v";

    public static Options OPTIONS = new Options();

    static {
        OPTIONS.addOption(OptionBuilder.hasArg().isRequired().withArgName(FILE_ARG_NAME).withType(String.class)
                .withLongOpt(FILE_LONG_OPTION).withDescription(FILE_OPTION_DESC).create(FILE_OPTION));

        OPTIONS.addOption(OptionBuilder.hasArg().isRequired().withArgName(OUT_ARG_NAME).withType(String.class)
                .withLongOpt(OUT_FILE_LONG_OPTION).withDescription(OUT_OPTION_DESC).create(OUT_FILE_OPTION));

        OPTIONS.addOption(OptionBuilder.withDescription(VERBOSE_OPTION_DESC).create(VERBOSE_OPTION));
    }

}
