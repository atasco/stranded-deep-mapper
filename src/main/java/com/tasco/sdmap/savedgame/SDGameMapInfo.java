package com.tasco.sdmap.savedgame;

import java.util.function.Consumer;

/**
 * @author atasco
 *
 */
public class SDGameMapInfo {
    private long minX = 0;
    private long maxX = 0;

    private long minY = 0;
    private long maxY = 0;

    private long originX;
    private long originY;

    private SDGameMapInfo() {

    }

    public long getOriginX() {
        return this.originX;
    }

    public void setOriginX(final long originX) {
        this.originX = originX;
    }

    public long getOriginY() {
        return this.originY;
    }

    public void setOriginY(final long originY) {
        this.originY = originY;
    }

    public long getMinX() {
        return this.minX;
    }

    public void setMinX(final long minX) {
        this.minX = minX;
    }

    public long getMaxX() {
        return this.maxX;
    }

    public void setMaxX(final long maxX) {
        this.maxX = maxX;
    }

    public long getMinY() {
        return this.minY;
    }

    public void setMinY(final long minY) {
        this.minY = minY;
    }

    public long getMaxY() {
        return this.maxY;
    }

    public void setMaxY(final long maxY) {
        this.maxY = maxY;
    }

    public static SDGameMapInfo createFromGameMap(final SDGameMap map) {
        final SDGameMapInfo mapInf = new SDGameMapInfo();

        map.getNodes().forEach(new Consumer<GameNode>() {
            @Override
            public void accept(final GameNode node) {
                if (node.getX() > mapInf.getMaxX()) {
                    mapInf.setMaxX(node.getX());
                }

                if (node.getX() < mapInf.getMinX()) {
                    mapInf.setMinX(node.getX());
                }

                if (node.getY() > mapInf.getMaxY()) {
                    mapInf.setMaxY(node.getY());
                }

                if (node.getY() < mapInf.getMinY()) {
                    mapInf.setMinY(node.getY());
                }
            }

        });

        return mapInf;
    }
}
