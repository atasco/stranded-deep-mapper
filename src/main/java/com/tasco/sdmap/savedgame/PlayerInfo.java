package com.tasco.sdmap.savedgame;

public class PlayerInfo {
    private final long offsetX;
    private final long offsetY;

    public PlayerInfo(final long x, final long y) {
        this.offsetX = x;
        this.offsetY = y;
    }

    public long getOffsetX() {
        return this.offsetX;
    }

    public long getOffsetY() {
        return this.offsetY;
    }

}
