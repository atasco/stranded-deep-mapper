package com.tasco.sdmap.savedgame;

public class GameNode {
    private long x;
    private long y;
    private long z;
    private String type;
    private boolean fullyGenerated;

    public boolean isFullyGenerated() {
        return this.fullyGenerated;
    }

    public void setFullyGenerated(final boolean fullyGenerated) {
        this.fullyGenerated = fullyGenerated;
    }

    public String getType() {
        return this.type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public long getX() {
        return this.x;
    }

    public void setX(final long x) {
        this.x = x;
    }

    public long getY() {
        return this.y;
    }

    public void setY(final long y) {
        this.y = y;
    }

    public long getZ() {
        return this.z;
    }

    public void setZ(final long z) {
        this.z = z;
    }
}
