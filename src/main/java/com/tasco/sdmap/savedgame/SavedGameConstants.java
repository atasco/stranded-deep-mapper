package com.tasco.sdmap.savedgame;

public class SavedGameConstants {
    public static final String PERSISTENT = "Persistent";
    public static final String TERRAIN_GENERATION = "TerrainGeneration";
    public static final String WORLD_ORIGIN_POINT = "WorldOriginPoint";
    public static final String PLAYER_POSITION = "playerPosition";
    public static final String NODES = "Nodes";
    public static final String BIOME = "biome";
    public static final String POSITION = "position";
    public static final String POSITION_OFFSET = "positionOffset";
    public static final String X = "x";
    public static final String Y = "z";
    public static final String POS_VAL_SPLIT = "f";
    public static final String ISLAND_BIOME = "ISLAND";
    public static final Object FULLY_GENERATED = "fullyGenerated";
}
