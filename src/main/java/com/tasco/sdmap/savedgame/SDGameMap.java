package com.tasco.sdmap.savedgame;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;

import com.tasco.sdmap.data.JsonSerializer;

public class SDGameMap {
    private final List<GameNode> nodes = new ArrayList<>();
    private SDGameMapInfo mapInfo;

    private PlayerInfo playerInfo;

    private SDGameMap() {}

    public List<GameNode> getNodes() {
        return this.nodes;
    }

    public SDGameMapInfo getMapInfo() {
        return this.mapInfo;
    }

    public void setMapInfo(final SDGameMapInfo mapInfo) {
        this.mapInfo = mapInfo;
    }

    public PlayerInfo getPlayerInfo() {
        return this.playerInfo;
    }

    public void setPlayerInfo(final PlayerInfo playerInfo) {
        this.playerInfo = playerInfo;
    }

    @SuppressWarnings("unchecked")
    public static SDGameMap createFromSaveFile(final Path saveFile) throws IOException {
        final SDGameMap gameMap = new SDGameMap();

        String jsonSaveData = "";

        try (final InputStream is = Files.newInputStream(saveFile, StandardOpenOption.READ);) {
            jsonSaveData = IOUtils.toString(is);
        } catch (final IOException e) {
            throw new IOException("Could not open save file for reading", e);
        }

        final Map<String, Object> saveData = JsonSerializer.deserializeToMap(jsonSaveData);

        jsonSaveData = "";

        final Map<String, Object> persistentData = (Map<String, Object>) saveData.get(SavedGameConstants.PERSISTENT);
        final Map<String, Object> terrainData = (Map<String, Object>) persistentData
                .get(SavedGameConstants.TERRAIN_GENERATION);
        final Map<String, Object> nodes = (Map<String, Object>) terrainData.get(SavedGameConstants.NODES);
        final Map<String, String> player = (Map<String, String>) terrainData.get(SavedGameConstants.PLAYER_POSITION);
        final Map<String, String> origin = (Map<String, String>) terrainData.get(SavedGameConstants.WORLD_ORIGIN_POINT);

        final PlayerInfo playerInfo = new PlayerInfo(
                parsePositionValue(player.get(SavedGameConstants.X).split("\\.")[0]), parsePositionValue(player.get(
                        SavedGameConstants.Y).split("\\.")[0]));

        for (final Entry<String, Object> nodeData : nodes.entrySet()) {
            final Map<String, Object> data = (Map<String, Object>) nodeData.getValue();

            final GameNode node = new GameNode();

            node.setType((String) data.get(SavedGameConstants.BIOME));

            final Map<String, String> positionOffset = (Map<String, String>) data
                    .get(SavedGameConstants.POSITION_OFFSET);

            node.setX(parsePositionValue(positionOffset.get(SavedGameConstants.X)));
            node.setY(parsePositionValue(positionOffset.get(SavedGameConstants.Y)));
            node.setFullyGenerated((boolean) data.get(SavedGameConstants.FULLY_GENERATED));

            gameMap.getNodes().add(node);
        }

        gameMap.setPlayerInfo(playerInfo);

        final SDGameMapInfo mapInf = SDGameMapInfo.createFromGameMap(gameMap);
        mapInf.setOriginX(parsePositionValue(origin.get(SavedGameConstants.X)));
        mapInf.setOriginY(parsePositionValue(origin.get(SavedGameConstants.Y)));
        gameMap.setMapInfo(mapInf);

        return gameMap;
    }

    public String serialize() throws IOException {
        return JsonSerializer.serializeToJson(this);
    }

    private static long parsePositionValue(String posVal) {
        posVal = posVal.split(SavedGameConstants.POS_VAL_SPLIT)[1];
        return Long.parseLong(posVal);
    }
}
