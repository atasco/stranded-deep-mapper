package com.tasco.sdmap.data;

import java.io.IOException;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;

public class JsonSerializer {
    @SuppressWarnings("unchecked")
    public static Map<String, Object> deserializeToMap(final String json) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.readValue(json, Map.class);
        } catch (final IOException e) {
            // cleaner error message
            throw new IOException("Could not read save file data", e);
        }
    }

    public static String serializeToJson(final Object object) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.writeValueAsString(object);
        } catch (final IOException e) {
            throw new IOException("Could not serialize object data", e);
        }
    }
}
