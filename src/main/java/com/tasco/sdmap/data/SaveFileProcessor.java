package com.tasco.sdmap.data;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import org.apache.commons.io.IOUtils;

import com.tasco.sdmap.savedgame.SDGameMap;

public class SaveFileProcessor {
    private final SDGameMap gameMap;

    public SaveFileProcessor(final Path saveFile) throws IOException {
        this.gameMap = SDGameMap.createFromSaveFile(saveFile);
    }

    public void generateVisualizationFile(final String outputFileName) throws IOException {
        final String jsonData = this.gameMap.serialize();

        final File outFile = new File(outputFileName);
        outFile.createNewFile();
        final Path path = outFile.toPath();

        try (OutputStream os = Files.newOutputStream(path, StandardOpenOption.WRITE);) {
            IOUtils.write(jsonData, os);
        } catch (final IOException e) {
            throw new IOException("Could not write output file", e);
        }
    }
}
