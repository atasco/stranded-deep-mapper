package com.tasco.sdmap.cli.options;

import org.apache.commons.cli.ParseException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CliParserTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static final String TEST_INPUT_PATH = "/home/brain-scorcher/Save.json";
    private static final String TEST_OUTPUT_PATH = "home/brain-scorcher/sd-mapper/output.json";

    private static final String[] GOOD_CLI_ARGS_VERBOSE = { "-" + CliOptions.FILE_OPTION, TEST_INPUT_PATH,
            "-" + CliOptions.OUT_FILE_OPTION, TEST_OUTPUT_PATH, "-" + CliOptions.VERBOSE_OPTION };

    private static final String[] GOOD_CLI_ARGS = { "-" + CliOptions.FILE_OPTION, TEST_INPUT_PATH,
            "-" + CliOptions.OUT_FILE_OPTION, TEST_OUTPUT_PATH };

    private static final String[] BAD_CLI_ARGS = { TEST_INPUT_PATH, "-" + CliOptions.OUT_FILE_OPTION, TEST_OUTPUT_PATH };

    @Test
    public void testShouldGetErrorWithBadCommandLine() throws ParseException {
        this.expectedException.expect(ParseException.class);
        CliParser.parseCliForSaveFile(BAD_CLI_ARGS);
    }

    @Test
    public void testCanParseInputSaveFilePath() throws ParseException {
        final String out = CliParser.parseCliForSaveFile(GOOD_CLI_ARGS);
        Assert.assertEquals(TEST_INPUT_PATH, out);
    }

    @Test
    public void testCanParseOutputFilePath() throws ParseException {
        final String out = CliParser.parseCliForOutputFileName(GOOD_CLI_ARGS);
        Assert.assertEquals(TEST_OUTPUT_PATH, out);
    }

    @Test
    public void testVerboseShouldEvaluateTrue() throws ParseException {
        Assert.assertTrue(CliParser.isVerboseSet(GOOD_CLI_ARGS_VERBOSE));
    }

    @Test
    public void testVerboseShouldEvaluateFalse() throws ParseException {
        Assert.assertFalse(CliParser.isVerboseSet(GOOD_CLI_ARGS));

    }
}
