package com.tasco.sdmap.cli.savedgame;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Test;

import com.tasco.sdmap.savedgame.SDGameMap;

public class CanLoadGameMapTest {

    @Test
    public void testCanCreateSDGameMapFromSaveFile() throws IOException, URISyntaxException {
        final SDGameMap map = SDGameMap.createFromSaveFile(new File(this.getClass().getResource("/Save.json").toURI())
                .toPath());

        // Map Info
        Assert.assertEquals(1024L, map.getMapInfo().getMaxX());
        Assert.assertEquals(-1536L, map.getMapInfo().getMinX());
        Assert.assertEquals(2304L, map.getMapInfo().getMaxY());
        Assert.assertEquals(-512L, map.getMapInfo().getMinY());
        Assert.assertEquals(0L, map.getMapInfo().getOriginX());
        Assert.assertEquals(1024L, map.getMapInfo().getOriginY());

        // Player Info
        Assert.assertEquals(3L, map.getPlayerInfo().getOffsetX());
        Assert.assertEquals(27L, map.getPlayerInfo().getOffsetY());

        // Nodes
        Assert.assertEquals(97, map.getNodes().size());

    }
}
